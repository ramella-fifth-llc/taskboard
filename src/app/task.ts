export interface Task {
  title: string;
  description: string;
  checked: boolean;
}

