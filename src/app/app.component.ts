import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Task} from './task';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
    public taskCreationForm: FormGroup;
    public taskList: Task[] = [
        {
            title: 'ghj',
            description: 'ghj',
            checked: false
        },
        {
            title: 'ghj2',
            description: 'ghj',
            checked: false
        },
        {
            title: 'ghj3',
            description: 'ghj',
            checked: false
        },
        {
            title: 'ghj4',
            description: 'ghj',
            checked: false
        },
    ];

    public completedItems = [];
    public resultArray = this.taskList;
    public optionList = ['Completed', 'Incomplete', 'All tasks'];

    constructor( private fb: FormBuilder) {

    }

    ngOnInit() {
        this.initForm();
    }

    public initForm() {
        this.taskCreationForm = this.fb.group({
            title: null,
            description: null,
            checked: false
        });
    }

    public createTask() {
        this.taskList.push(this.taskCreationForm.getRawValue());
    }

    public toggleCheck(checked: boolean, item: any) {
        if (checked) {
            item.checked = true;
            this.completedItems.push(item);

        } else {
            item.checked  = false;
            this.completedItems.splice(this.completedItems.indexOf(item), 1);
        }
    }

    public filterTaskTypes(value: string) {
        switch (value) {
            case 'Completed':
                this.resultArray = this.taskList.filter(item => this.completedItems.includes(item));
                // console.log( this.resultArray , 'Completed');
                break;
            case 'Incomplete': {
                this.resultArray = this.taskList.filter(item => !this.completedItems.includes(item));
                // console.log( this.resultArray , 'Incomplete');
                break;
            }
            case 'All tasks':
                this.resultArray =  this.taskList;
                // console.log( this.resultArray , 'All tasks');
                break;
        }
    }
}
