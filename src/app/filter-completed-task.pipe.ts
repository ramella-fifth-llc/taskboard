import {Pipe, PipeTransform} from '@angular/core';

import { Task } from './task';

@Pipe({ name: 'filterCompletedTaskPipe'})
export class FilterCompletedTaskPipe implements PipeTransform {
    transform(allTasks: Task[]) {
        return allTasks;
    }
}
