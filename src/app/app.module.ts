import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FilterCompletedTaskPipe} from './filter-completed-task.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FilterCompletedTaskPipe,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
